# Build stage0 and git clonr service checks
FROM python:3.9-alpine3.15 AS stage0

ARG user
ARG token
ARG repo
ARG branch="main"

COPY .dockerignore .


RUN apk -U upgrade --no-cache \
    && \
    apk add --no-cache --update \
    ca-certificates \
    git \
    build-base \
    openssl-dev \
    libffi-dev \
    && \
    git clone --branch ${branch} https://${user}:${token}@${repo} \
    && \
    cd scripts/ \
    && \
    pip install -U pip wheel \
    && \
    pip wheel -r requirements.txt --wheel-dir=/tmp/wheels
    





# Build app using assets from stage0
FROM alpine AS app

ARG NAME="service-checker"
ARG DESCRIPTION="Scoreboard service check worker container"
ARG MAINTAINER="Lucas Halbert <lhalbert@c2games.org>"
ARG VENDOR="c2games"
ARG ARCH=amd64
ARG DOCKER_CMD="docker run -t --rm registry.gitlab.com/c2-games/ctf/scoring/service-checks/container"
ARG DOCKER_CMD_HELP="N/A"
ARG IMAGE_USAGE="https://gitlab.com/c2-games/ctf/scoring/service-checks/container/-/blob/main/README.md"
ARG VCS_URL="https://gitlab.com/c2-games/ctf/scoring/service-checks/container"
ARG VCS_REF
ARG VCS_TIMESTAMP
ARG VERSION
ARG BUILD_DATE

ENV CONFIG_API_URI=https://api.scoring.cnyhackathon.org/v1/
ENV SERVICE=icmp
#ENV SERVICE_CHECK_SCRIPT=check_icmp.sh
ENV API_AUTH=TRUE
#ENV API_URI=https://api.scoring.cnyhackathon.org/v1/
ENV API_USER=serviceChecker
ENV API_TOKEN=aabbccdd1234
#ENV TEAM_SUBNET_SCHEME=172.18.13
#ENV TEAMS=0,1,2,3,4,5
#ENV MIN_RAND=0
#ENV MAX_RAND=59
#ENV CHECK_INTERVAL=1
ENV SEND_TO_API=TRUE
ENV WRITE_TO_LOG_FILE=TRUE
ENV WRITE_TO_STDOUT=FALSE

MAINTAINER Lucas Halbert <lhalbert@c2games.org>
LABEL org.label-schema.maintainer=${MAINTAINER} \
    org.label-schema.build-date=${BUILD_DATE} \
    org.label-schema.name=${NAME} \
    org.label-schema.vendor=${VENDOR} \
    org.label-schema.description=${DESCRIPTION} \
    org.label-schema.version=${VERSION} \
    org.label-schema.architecture=${ARCH} \
    org.label-schema.vcs-ref=${VCS_REF} \
    org.label-schema.vcs-timestamp=${VCS_TIMESTAMP} \
    org.label-schema.vcs-url=${VCS_URL} \
    org.label-schema.docker.cmd=${DOCKER_CMD} \
    org.label-schema.docker.cmd.help=${DOCKER_CMD_HELP} \
    org.label-schema.usage=${IMAGE_USAGE} \
    org.label-schema.schema-version="1.0"


COPY --from=stage0 /scripts/[a-zA-Z0-9]* /opt/scripts/
COPY --from=stage0 /tmp/wheels /opt/wheels
ADD https://github.com/just-containers/s6-overlay/releases/download/v2.2.0.1/s6-overlay-amd64.tar.gz /tmp/
COPY ./ /

RUN apk -U upgrade --no-cache \
    && \
    apk add --no-cache --update \
    ca-certificates \
    curl \
    jq \
    bash \
    python3 \
    shadow \
    iputils \
    tzdata \
    openssh \
    grep \
    sed \
    py3-pip \
    py3-dnspython \
    wget \
    lftp \
    mysql-client \
    bind-tools \
    && \
    python3 -m pip install -U pip \
    && \
    python3 -m pip install --no-index --find-links=/opt/wheels -r /opt/scripts/requirements.txt \
    && \
    tar xzf /tmp/s6-overlay-amd64.tar.gz -C / \
    && \
    rm /tmp/s6-overlay-amd64.tar.gz \
    && \
    groupmod -g 1000 users \
    && \
    useradd -u 911 -U -d /opt/scripts -s /bin/false abc \
    && \
    usermod -G users abc \
    && \
    chown -R abc:abc /opt/scripts \
    && \
    chmod 0740 /opt/scripts/*

ENTRYPOINT  ["/init"]
CMD []
