#!/usr/bin/with-contenv /bin/bash
POD_ID=${HOSTNAME:-unknown-pod-name}
POD_IP=$(ip -4 -o addr show eth0 | awk '{print $4}')
SERVICE_CHECK=$(echo ${SERVICE_CHECK_SCRIPT} | sed -e 's/check_//' -e 's/\.[a-z]\+$//')
SERVICE_ID=${SERVICE_ID:=0000000}
SERVICE=${SERVICE:-unknown-service}
SCORABLE_EVENT=${SCORABLE_EVENT:-TRUE}
LOG_PATH=/tmp/${SERVICE_CHECK_SCRIPT}.out
API_URI=${API_URI:-https://api.scoring.cnyhackathon.org/v1/}
API_AUTH=${API_AUTH:-TRUE}
SEND_TO_API=${SEND_TO_API:-TRUE}
WRITE_TO_LOG_FILE=${WRITE_TO_LOG_FILE:-TRUE}
SELF_DESTRUCT=${SELF_DESTRUCT:-FALSE}
DATETIME='date "+%Y-%m-%d %H:%M:%S"'
TIME='date +%FT%T.00Z'
INTERVAL=${INTERVAL:-$(date +%S)}
PERIOD="date +%FT%H:%M:${INTERVAL}.00Z"



function initializeCheck() {
    local team_id=$1
    local team_name=$2
    local team_ip=$3
    local sleep_time=$(shuf -i${MIN_RAND}-${MAX_RAND} -n1)
    echo $(eval ${DATETIME}) - Service Checker - INFO - Sleeping for ${sleep_time} seconds before executing ${team_name} ${SERVICE} check
    sleep ${sleep_time}
    if [[ "${SERVICE_CHECK_SCRIPT_ARGS}" == "null" ]]; then
        local service_result_json=$(/opt/scripts/${SERVICE_CHECK_SCRIPT} ${team_ip})
    else
        local service_result_json=$(/opt/scripts/${SERVICE_CHECK_SCRIPT} ${team_ip} $(eval echo "${SERVICE_CHECK_SCRIPT_ARGS}"))
    fi

    local json=$(formatJson ${team_id} ${team_ip} "${service_result_json}")

    if [[ "${WRITE_TO_STDOUT}" == "TRUE" ]]; then
        writeToStdout "${json}" "${team_name}"
    fi

    if [[ "${WRITE_TO_LOG_FILE}" == "TRUE" ]]; then
        writeToLogFile "${json}" "${team_name}"
    fi

    if [[ "${SEND_TO_API}" == "TRUE" ]]; then
        submitToAPI "${json}" "${team_name}"
    fi
}


function formatJson() {
    local team_id=$1
    local team_ip=$2
    local service_result_json=$3
    local event_time=$(eval ${TIME})
    local event_period=$(eval ${PERIOD})
    local service_id=${SERVICE_ID}

    local event_result=$(echo "${service_result_json}" | jq ".result")
    local extended_data=$(echo "${service_result_json}" | jq ".extendedData")

    jq --arg event_time "${event_time}" \
    --arg event_period "${event_period}" \
    --arg team_id "${team_id}" \
    --arg team_ip "${team_ip}" \
    --arg event_type "service" \
    --arg event_result "${event_result}" \
    --arg pod_id "${POD_ID}" \
    --arg pod_ip "${POD_IP}" \
    --arg service_id "${service_id}"  \
    --arg scorable_event "${SCORABLE_EVENT}"  \
    --argjson extended_data "${extended_data}" \
    -n '{ object: {event_time: $event_time, event_period: $event_period, event_type: $event_type, event_result: $event_result, team_id: $team_id, team_ip: $team_ip, pod_id: $pod_id, pod_ip: $pod_ip, service_id: $service_id, scorable_event: $scorable_event, extended_data: $extended_data} }'
}


function writeToStdout() {
    local json=$1
    local team_name=$2
    echo -e $(eval ${DATETIME}) - Service Checker - ${team_name} INFO - ${json}
}


function writeToLogFile() {
    local json=$1
    local team_name=$2
    echo $(eval ${DATETIME}) - Service Checker - ${team_name} INFO - ${json} >> ${LOG_PATH}
}


function submitToAPI() {
    local json=$1
    local team_name=$2

    # Verify that Api Token is still valid
    getApiToken

    # All thanks srad for the hacky AF octal character injection
    if [[ "${API_AUTH}" == "TRUE" ]]; then
        response=$(curl -w "$(echo -en "\001")%{response_code}" -s -H "Authorization: Bearer ${API_BEARER_TOKEN}" -H "Accept: application/json" -H "Content-Type: application/json" -X POST -d "${json}" ${API_URI})
    else
        response=$(curl -w "$(echo -en "\001")%{response_code}" -s -H "Accept: application/json" -H "Content-Type: application/json" -X POST -d "${json}" ${API_URI})
    fi

    # Curl return code
    rc=$?

    status_code=$(echo -n "${response}" | sed 's/\(^.*\)\o001\([0-9]\{3\}\)$/\2/g')
    json=$(echo -n "${response}" | sed 's/\(^.*\)\o001\([0-9]\{3\}\)$/\1/g')

    if [[ ${rc} -ne 0 ]]; then
        echo "$(eval ${DATETIME}) - Service Checker - ERROR - ${SERVICE} check result for ${team_name} failed - code ${rc}"
        exit 23
    fi

    if [[ ${status_code} -ne 200 ]]; then
        echo "$(eval ${DATETIME}) - Service Checker - ERROR - ${SERVICE} check result for ${team_name} failed - HTTP/${status_code}"
	echo "$(eval ${DATETIME}) - Service Checker - DEBUG - ${json}"
        exit 24
    fi

    echo "$(eval ${DATETIME}) - Service Checker - INFO - ${SERVICE} check result for ${team_name} Succeeded"

}


function getApiToken() {
    if [[ -f /var/run/s6/container_environment/API_BEARER_TOKEN} ]]; then
        echo "$(eval ${DATETIME}) - Service Checker - INFO - No valid API token available. Attemptin to fetching a new API Token"
        refreshApiToken
    fi

    if [[ $(date +%s) -gt $(head -n 1 /var/run/s6/container_environment/API_BEARER_TOKEN_EXPIRATION) ]]; then
        echo "$(eval ${DATETIME}) - Service Checker - INFO - API Token has expired. Attempting to refresh."
        refreshApiToken
    fi
}


function refreshApiToken() {

    echo "$(eval ${DATETIME}) - Service Checker - INFO - Refreshing API bearer token"

    # Fetch Access token using base64 client_id:client_secret
    local response=$(curl -w "$(echo -en "\001")%{response_code}" \
      -s --request POST \
      --retry 5 \
      --connect-timeout 5 \
      --url ${AUTH_URL} \
      --header "Content-Type: application/x-www-form-urlencoded" \
      --header "Authorization: Basic $(echo ${client_id}:${client_secret} | tr -d '\n' | base64)" \
      --data grant_type=client_credentials)

    # Curl return code
    local rc=$?

    local status_code=$(echo -n "${response}" | sed 's/\(^.*\)\o001\([0-9]\{3\}\)$/\2/g')
    local json=$(echo -n "${response}" | sed 's/\(^.*\)\o001\([0-9]\{3\}\)$/\1/g')

    if [[ ${rc} -ne 0 ]]; then
        echo "$(eval ${DATETIME}) - Service Checker - ERROR - There was a problem fetching the access token from keycloak - ${rc}"
        selfDestructPod
        exit 1
    fi

    if [[ ${status_code} -ne 200 ]]; then
        echo "$(eval ${DATETIME}) - Service Checker - ERROR - There was a problem fetching the access token from keycloak - ${status_code}"
        echo "$(eval ${DATETIME}) - Service Checker - DEBUG - ${json}"
        selfDestructPod
        exit 24
    fi

    echo "$(eval ${DATETIME}) - Service Checker - INFO - API bearer token was fetched successfully"


    export API_BEARER_TOKEN=$(echo "${json}" | jq -r '.access_token' | tr -d "\n")

    # Check that an access token was successfully fetched
    if [[ "${API_BEARER_TOKEN}" == "null" ]]; then
        echo "$(eval ${DATETIME}) - Service Checker - ERROR - There was a problem extracting a valid access token from the JSON."
        echo "$(eval ${DATETIME}) - Service Checker - DEBUG - ${json}"
        selfDestructPod
        exit 2
    fi

    # set token expiration to current time + expires_in (seconds) - 5 (minutes)
    export API_BEARER_TOKEN_EXPIRATION=$(date +%s --date="@$(($(date +%s) + $(echo "${json}" | jq -r '.expires_in') - 300))" |  tr -d "\n")

    echo -n ${API_BEARER_TOKEN} > /var/run/s6/container_environment/API_BEARER_TOKEN
    echo -n ${API_BEARER_TOKEN_EXPIRATION} > /var/run/s6/container_environment/API_BEARER_TOKEN_EXPIRATION
}


function selfDestructPod() {
    echo "$(eval ${DATETIME}) - Service Checker - INFO - The pod will now self destruct"
    curl -m 10 -k -X DELETE \
      --data '{"gracePeriodSeconds":0,"propagationPolicy":"Background"}' \
      -H "Authorization: Bearer $(cat /run/secrets/kubernetes.io/serviceaccount/token)" \
      -H 'Accept: application/json' \
      -H'Content-Type: application/json' https://10.96.0.1/api/v1/namespaces/$(cat /run/secrets/kubernetes.io/serviceaccount/namespace)/pods/$(hostname)

}

function check_networking_and_self_destruct {
	first_three_octets=$(ip -4 addr show net1 | grep inet | awk '{print $2}' | cut -d . -f -3)

	if echo $first_three_octets | grep -q '172\.18\.'
	then
		check_ip=172.18.0.1
	elif echo $first_three_octets | grep -q '192\.168\.'
	then
		check_ip=${first_three_octets}.55
	else
		# Don't kill the whole script if the ip isn't what we expect so scoring
		# might still continue to function.
		echo "$first_three_octets IP is not recognized baling from network check"
		return
	fi

	if ! ping -q -c 1 -W 1 $check_ip
	then
		echo "Cant reach $check_ip self destructing"
		selfDestructPod
	fi
}

check_networking_and_self_destruct

# Convert TEAMS and SERVICE_IPS into array
TEAM_IDS=($(echo $TEAM_IDS | tr "," " "))
TEAM_NAMES=($(echo $TEAM_NAMES | tr "," " "))
SERVICE_IPS=($(echo $SERVICE_IPS | tr "," " "))
SUBNETS=($(echo $SUBNETS | tr "," " "))


for (( i=0; i<${#TEAM_IDS[@]}; i++)); do
    initializeCheck ${TEAM_IDS[${i}]} ${TEAM_NAMES[${i}]} ${SERVICE_IPS[${i}]} &
done

# Impliment self-destruct feature
if [[ "${SELF_DESTRUCT}" == "TRUE" ]]; then
    #if [[ ! -f "/opt/scripts/SELF_DESTRUCT_COUNTDOWN" ]]; then
    if [[ -z "${SELF_DESTRUCT_COUNTDOWN}" ]]; then
        SELF_DESTRUCT_MIN_RAND=${SELF_DESTRUCT_MIN_RAND:=10}
        SELF_DESTRUCT_MAX_RAND=${SELF_DESTRUCT_MAX_RAND:=25}
        SELF_DESTRUCT_COUNTDOWN=${SELF_DESTRUCT_COUNTDOWN:- $(shuf -i${SELF_DESTRUCT_MIN_RAND}-${SELF_DESTRUCT_MAX_RAND} -n1)}
#    else
#        SELF_DESTRUCT_COUNTDOWN=$(head -n 1 /opt/scripts/SELF_DESTRUCT_COUNTDOWN)
    fi


    if [[ ((${SELF_DESTRUCT_COUNTDOWN} -le 0)) ]]; then
        # wait for all background processes to complete and kill pod
        echo $(eval ${DATETIME}) - Service Checker - INFO - Waiting for all background processes to complete before self destructing
        wait

        # execute call to terminate pod
        selfDestructPod
        #curl -m 10 -k -X DELETE --data '{"gracePeriodSeconds":0,"propagationPolicy":"Background"}' -H "Authorization: Bearer $(cat /run/secrets/kubernetes.io/serviceaccount/token)" -H 'Accept: application/json' -H'Content-Type: application/json' https://kubernetes.default/api/v1/namespaces/$(cat /run/secrets/kubernetes.io/serviceaccount/namespace)/pods/$(hostname)
    else
        echo $(eval ${DATETIME}) - Service Checker - INFO - Pod will self destruct in ${SELF_DESTRUCT_COUNTDOWN} iterations

        # Write countdown to environment variable
        #echo $((${SELF_DESTRUCT_COUNTDOWN}-1)) > /opt/scripts/SELF_DESTRUCT_COUNTDOWN
        echo $((${SELF_DESTRUCT_COUNTDOWN}-1)) > /var/run/s6/container_environment/SELF_DESTRUCT_COUNTDOWN
    fi
fi
