# Scoring Service-check Container

The scoring service-check container uses alpine linux in conjuction with cron and a random number generator to initiate checks of each team's infrastructure at a random interval between `${MIN_RAND}` and `${MAX_RAND}` seconds. This time period is configurable using the environment variables listed.

# Build the image
```sh
docker build --build-arg BUILD_DATE=$(date --utc +%FT%T.%2NZ) \
--build-arg VCS_REF=$(git rev-parse --short HEAD) \
--build-arg VERSION=v0.1.1 \
--build-arg VCS_TIMESTAMP=$(date -d @$(git log -1 --format=%at) --utc +%FT%T.%2NZ) \
--build-arg user=**REDACTED** \
--build-arg token=**REDACTED** \
--build-arg repo=gitlab.com/c2-games/ctf/scoring/service-checks/scripts.git \
-t scoring-container:v0.1.1 .
```



## Container environment variables
* `PUID=8675309` - The UID used to execute the service check script.
* `PGID=8675309` - The GID used to execute the service check script.
* `TZ="America/New_York"` - Sets the timezone of the container.
* `SERVICE_CHECK_SCRIPT=check_icmp.sh` - Service check scripts are maintained within the [c2-games/ctf/scoring/service-checks/scripts](https://gitlab.com/c2-games/ctf/scoring/service-checks/scripts) repository. These scripts are pulled at container build time.
* `CHECK_INTERVAL=1` - Used to configure how often the cron fires (integer is in minutes)
* `MIN_RAND=0` - Used in the random number generator to configure the minimum number of seconds after cron fires to execute service check 
* `MAX_RAND=55` - Used in the random number generator to configure the maximum number of seconds after cron fires to execute service check 
* `SEND_TO_API=TRUE` - (TRUE or FALSE): used to enable/disable sending results to the API
* `API_AUTH=TRUE` - (TRUE or FALSE): used to enable/disable API authentication
* `API_URI=https://api.scoring.cnyhackathon.org/v1/` - URI of the scoring engine API (currently under development)
* `API_USER=serviceChecker` - User to authenticate to the API with
* `API_TOKEN=abcdefg123456` - Token to use in conjuction with the `API_USER` to authenticate with the API
* `TEAM_SUBNET_SCHEME=172.18.13` - This is the first 3 octets used to denote each team's router IP
* `TEAMS=0,1,3,7` - Comma separated list of teams used to denote which teams the service check should execute against
* `WRITE_TO_LOG_FILE=TRUE` - (TRUE or FALSE): used to enable/disable writing JSON results to the local log file located at `/tmp/${SERVICE_CHECK_SCRIPT}.out` 
* `WRITE_TO_STDOUT=TRUE` - (TRUE or FALSE): used to enable/disable writing JSON results to stdout
* `SELF_DESTRUCT=TRUE` - (TRUE or FALSE): used to enable/disable self destruct function
* `SELF_DESTRUCT_MIN_RAND=1` - used in the random number generator to configure thr number of iterations before pod will self destruct.
* `SELF_DESTRUCT_MAX_RAND=15` - used in the random number generator to configure thr number of iterations before pod will self destruct.


### Expected JSON Object Format for API Submission
```json
{
    "dateTime": "2021-02-15 10:32:11",
    "teamId": "0",
    "teamIp": "172.18.13.0",
    "podId": "9b96bef8c92f",
    "podIp": "172.17.0.2/16"
    "serviceCheck": "icmp",
    "data": {
        "result": 0,
        "extendedData": {
            "packetsTransmitted": "1",
            "packetsReceived": "1",
            "packetLoss": "0%",
            "totalErrors": "+0",
            "executionTime": "0ms",
            "rttMin": "1.287",
            "rttAvg": "1.287",
            "rttMax": "1.287",
            "rttMdev": "0.000"
        }
    }
}
```


## Docker Deployment Example
```sh
docker create --name ICMP-service-checker \
    -e PUID=8675309 \
    -e PGID=8675309 \
    -e TZ="America/New_York" \
    -e SERVICE_CHECK_SCRIPT=check_icmp.sh \
    -e CHECK_INTERVAL=1 \
    -e MIN_RAND=5 \
    -e MAX_RAND=50 \
    -e SEND_TO_API=TRUE \
    -e API_AUTH=FALSE \
    -e API_URI=https://api.scoring.cnyhackathon.org/v1/ \
    -e API_USER=serviceChecker \
    -e API_TOKEN=abcdefg123456 \
    -e TEAM_SUBNET_SCHEME=172.18.13 \
    -e TEAMS=0,1,3,7,8,9 \
    -e WRITE_TO_LOG_FILE=TRUE \
    -e WRITE_TO_STDOUT=TRUE \
    registry.gitlab.com/c2-games/ctf/scoring/service-checks/container:latest

docker start ICMP-service-checker

docker logs -f ICMP-service-checker

docker exec -it ICMP-service-checker tail -f /tmp/check_icmp.sh.out
```


## Kubernetes Deployment Example
Note the following custom directives for dropping pod onto a separate VLANed Subnet
`.spec.template.metadata.annotations.ovn.kubernetes.io/logical_switch`
`.spec.template.spec.containers.dnsConfig.nameservers`
`.spec.template.spec.containers.dnsConfig.searches`

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  labels:
    app: icmp-service-checker
  name: icmp-service-checker
  namespace: lhalbert-test
spec:
  progressDeadlineSeconds: 600
  replicas: 0
  revisionHistoryLimit: 10
  selector:
    matchLabels:
      app: icmp-service-checker
  strategy:
    rollingUpdate:
      maxSurge: 25%
      maxUnavailable: 25%
    type: RollingUpdate
  template:
    metadata:
      annotations:
        ovn.kubernetes.io/logical_switch: hackathon
      labels:
        app: icmp-service-checker
    spec:
      containers:
      - env:
        - name: PUID
          value: "8675309"
        - name: PGID
          value: "8675309"
        - name: TZ
          value: America/New_York
        - name: SERVICE_CHECK_SCRIPT
          value: check_icmp.sh
        - name: CHECK_INTERVAL
          value: "1"
        - name: MIN_RAND
          value: "0"
        - name: MAX_RAND
          value: "55"
        - name: SEND_TO_API
          value: "TRUE"
        - name: API_AUTH
          value: "FALSE"
        - name: API_URI
          value: https://api.scoring.cnyhackathon.org/v1/
        - name: API_USER
          value: serviceChecker
        - name: API_TOKEN
          value: abcdefg123456
        - name: TEAM_SUBNET_SCHEME
          value: 172.18.13
        - name: TEAMS
          value: "0"
        - name: WRITE_TO_LOG_FILE
          value: "FALSE"
        - name: WRITE_TO_STDOUT
          value: "FALSE"
        image: registry.gitlab.com/c2-games/ctf/scoring/service-checks/container:latest
        imagePullPolicy: Always
        name: container
        resources: {}
        terminationMessagePath: /dev/termination-log
        terminationMessagePolicy: File
      dnsConfig:
        nameservers:
        - 172.18.0.12
        searches:
        - cnyhackathon.org
      dnsPolicy: ClusterFirst
      imagePullSecrets:
      - name: gitlab-auth
      restartPolicy: Always
      schedulerName: default-scheduler
      securityContext: {}
      terminationGracePeriodSeconds: 30
status: {}
```
